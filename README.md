# OMOP-in-a-Baguette

## Objectifs

Disposer de datasets au format OMOP partageables au sein de la communauté des données de santé en France. Les usages potentiels :

- Formations / _toy datasets_ 🎓
- Collaborations sans exposer de données sensibles 🤝
- Tests de charge 🏋️‍♀️
- Comparaison de méthodes et d'outils sur des bases communes :bar_chart:
## Wishlist

- [ ] API de synthèse basée sur synthéa (format OMOP et FHIR, stockages CSV, JSON, Postgres)
- Déploiement : 
    - [ ] [Ansible](https://www.ansible.com/) ?
    - [ ] [Terraform](https://www.terraform.io/) ?

## Minimum viable product (MVP)

Pour le moment, ce repo contient un Docker compose qui articule les conteneurs suivants:

- `db`: base de données Postgres qui contient les données natives + post-ETL (username == password == database == `docker`)
- `synthea`: crée les données synthétiques à partir de Synthea, en intégrant les caractéristiques démographiques françaises (pour le moment crée 1000 patients par défaut)
- `etl`: transforme les données Synthea au format OMOP (v6 pour l'instant) et les verse dans `db`

Pour build le projet, exécutez la commande suivante (le conteneur `synthea` met du temps à se lancer la première fois, car beaucoup de tests sont effectués) :

```
docker-compose build --build-arg NB_PATIENTS=100 --build-arg ZONE=Île-de-france
```

Ensuite il suffit d'exécuter la commande suivante pour lancer la pipeline et avoir un base de données requêtable :

```
docker-compose up
```

Pour interagir avec la base de données, on peut lancer des requêtes de la forme suivante :

```
psql "postgresql://docker:docker@localhost:5432" --command "SELECT * FROM cdm_synthea10.visit_occurrence;"
```

### To-do

- [ ] Permettre le paramétrage de Synthea (nb de patients à créer, pathologies, etc.)
- [ ] API pour requêter la base de données
- [ ] Faire fonctionner OMOP v5.3.1

## Contexte

### Les datasets publics de dossiers médicaux électroniques

#### [CMS DE-SynPUF](https://www.cms.gov/Research-Statistics-Data-and-Systems/Downloadable-Public-Use-Files/SynPUFs/DE_Syn_PUF)

- Taille : 2.33M patients "synthétiques"
- Échantillon de bénéficiaires Medicare 2008-2010 dé-identifiés
- ETL vers OMOP v5.2 disponible [sur GitHub](https://github.com/OHDSI/ETL-CMS)
- Échantillon 1k OMOP v6 disponible [sur le Framagit d'interhop](https://framagit.org/interhop/challenges/datathon_dataset)

#### [Synthea](https://synthea.mitre.org/about)

Générateur de données synthétiques réalistes ([site web](https://synthea.mitre.org/about)) avec API FHIR. Les données sont obtenues à l'aide d'un [système modulaire et paramétrable de pathologies](https://github.com/synthetichealth/synthea/wiki)

- ETL vers OMOP v6 et v5.3.1 disponibles [sur GitHub](https://github.com/OHDSI/ETL-Synthea)

### Les datasets non publics, à accès facilité pour la recherche

- [NIS Database](https://hcup-us.ahrq.gov/db/nation/nis/nisdbdocumentation.jsp)
- [All of us project](https://www.researchallofus.org/about-the-research-hub/?_ga=2.142481269.659013912.1611609571-2095955845.1611224603) : Une cohorte de 1 millions de patients US en cours de constitution avec données

## Discutons !

Un canal de discussion Matrix à été ouvert, [rejoignez-nous y](https://matrix.to/#/!unyeFLaQiiOgcAxUrb:matrix.interhop.org?via=matrix.interhop.org) !
